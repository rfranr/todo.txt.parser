﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ConsoleApp1
{

    static public class Values
    {
        public static string[] states = { "x", " ", "-" };
        public static string[] priorities = { "(A)", "(B)", "(C)", "(Z)" };
        public static int valueIn ( string value, string[] values )
        {
            for ( int i=0;i<values.Length;i++)
                if (value.Equals(values[i]))
                    return i;
            return -1;
        }
    }

    class FilterCriteria
    {
        public enum Fields { state, priority, initDate, endDate, title, description, project, context };
        public enum Operator { GreatherThan, LessThan, Equal, NotEqual };
        public string ValueLeft  { get; set; }
        public string ValueRight { get; set; }
        public Fields Criteria { get; set; }
        public Operator Condition { get; set; }
        public override string ToString(){
            return "filter by : \n" + 
                Enum.GetName(typeof(Fields), Criteria) + "\n" +
                Enum.GetName(typeof(Operator), Condition) + "\n" +
                ValueLeft  + "\n" +
                ValueRight + "\n";
        }
    }

    enum ParserState { empty, state, priority, iniDate, endDate, title, description, project, context, comment, end }
    enum StateTask { done, working, standby }        
    enum PriorityTask { A,B,C,D,E,Z }


    class Line
    {
        public StateTask State { get; set; }
        public PriorityTask Priority { get; set; }
        public DateTime InitDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Project { get; set; }
        public string Context { get; set; }

        public double Minutes ()
        {
            return EndDate.TimeOfDay.TotalMinutes - InitDate.TimeOfDay.TotalMinutes;
        }

        public Line(LineParsed l)
        {
            try
            {
                int s = Values.valueIn(l.State, Values.states);
                switch (s)
                {
                    case 0: State = StateTask.done; break;
                    case 1: State = StateTask.working; break;
                    case 2: State = StateTask.standby; break;
                    default: throw new Exception("parse error ");
                }
                s = Values.valueIn(l.Priority, Values.priorities);
                switch (s)
                {
                    case 0: Priority = PriorityTask.A; break;
                    case 1: Priority = PriorityTask.B; break;
                    case 2: Priority = PriorityTask.C; break;
                    case 3: Priority = PriorityTask.Z; break;
                    default: throw new Exception("parse error ");
                }

                CultureInfo MyCultureInfo = new CultureInfo("es-ES");
                InitDate = DateTime.Parse(l.InitDate.Trim(), MyCultureInfo);
                EndDate = DateTime.Parse(l.EndDate.Trim(), MyCultureInfo);
                Title = l.Title;
                Description = l.Description;
                Project = l.Project.Trim();
                Context = l.Context?.Trim();
            }
            catch ( Exception ex )
            {
                throw new Exception(l.ToString(), ex);
            }
        }

        public override string ToString()
        {
            return "" +
             "State := " + State + ";\n" +
             "Priority := " + Priority + ";\n" +
             "InitDate := " + InitDate + ";\n" +
             "Minutes:= " + Minutes() + ";\n" +
             "EndDate := " + EndDate + ";\n" +
             "Title := " + Title + ";\n" +
             "Description := " + Description + ";\n" +
             "Project := " + Project + ";\n" +
             "Context := " + Context + ";";
        }

    }

    class LineParsed
    {
        public string State { get; set; }
        public string Priority { get; set; }
        public string InitDate { get; set; }
        public string EndDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Project { get; set; }
        public string Context { get; set; }
        public override string ToString() { return "State :" + this.State + "\n" + "Priority :" + this.Priority + "\n" + "InitDate :" + this.InitDate + "\n" + "EndDate :" + this.EndDate + "\n" + "Title :" + this.Title + "\n" + "Description :" + this.Description + "\n" + "Project :" + this.Project + "\n" + "Context :" + this.Context + "\n"; }
    }

    class Parser
    {

        private void Debug ( string message )
        {
            Console.WriteLine(message);
        }

        /// <summary>
        ///     Parse file
        /// </summary>
        /// <param name="filePath">Path of the file</param>
        /// <returns>List<Line></returns>
        public List<Line> ParseFile (string filePath )
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Open);
            List<Line> tasks = new List<Line>();
            using (StreamReader reader = new StreamReader(fileStream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Line task = parseLine(line);
                    if (task != null)
                        tasks.Add(task);
                }
            }
            return tasks;
        }

        /// <summary>
        /// Parsea una linea de fichero
        /// </summary>
        /// <param name="line">La linea de fichero</param>
        /// <returns>
        /// Line or Null ( if blank or comment )
        /// </returns>
        private Line parseLine ( string line  )
        {

            Console.WriteLine("|" + line + "|");
            if ( line == null )
                return null;
            if ( line?.Trim() == "" )
                return null;


            char prev = ' ', actual = ' ';
            string token = "";
            int times = 0;
            ParserState pState = ParserState.empty;
            LineParsed task = new LineParsed();
            for (int i = 0; i < line.Length; i++)
            {
                prev = actual;
                token = token + line[i];
                actual = line[i];
                switch (pState)
                {
                    case ParserState.empty:
                        if (actual == '/' && line[i+1] == '/' ) {
                            pState = ParserState.comment;
                        }
                        else { 
                            pState = ParserState.state;
                        }
                        break;
                    case ParserState.state:
                        if (actual == '(')
                        {
                            task.State = token.Substring(0,token.Length-2);
                            token = "";
                            pState = ParserState.priority;
                            i--;
                        }
                        break;
                    case ParserState.priority:
                        if (actual == ')')
                        {
                            task.Priority = token;
                            token = "";
                            pState = ParserState.iniDate;
                        }
                        break;
                    case ParserState.iniDate:
                        if (actual == ' ') times++;
                        if (actual == ' ' && times == 3)
                        {
                            task.InitDate = token;
                            token = ""; times = 0;
                            pState = ParserState.endDate;
                        }
                        break;
                    case ParserState.endDate:
                        if (actual == ' ') times++;
                        if (actual == ' ' && times == 2)
                        {
                            task.EndDate = token;
                            token = ""; times = 0;
                            pState = ParserState.title;
                        }
                        break;
                    case ParserState.title:
                        if (actual == '[') times = 1;
                        if (actual == ']' && prev != '\\' && times == 1)
                        {
                            task.Title = token;
                            token = ""; times = 0;
                            pState = ParserState.description;
                        }
                        break;
                    case ParserState.description:
                        if (actual == '+' && prev != '\\')
                        {
                            task.Description = token.Substring(0, token.Length - 1);
                            token = "";
                            pState = ParserState.project;
                            i--;
                        }
                        break;
                    case ParserState.project:
                        if ( (actual == '@' && prev != '\\') || (i == (line.Length - 1)) )
                        {
                            if (actual == '@')
                            {
                                task.Project = token.Substring(0, token.Length - 1);
                                i--;
                                pState = ParserState.context;
                            }
                            else
                            {
                                task.Project = token;
                                pState = ParserState.end;
                            }
                            token = "";
                        }
                        break;
                    case ParserState.context:
                        if (actual == ' ' || i == (line.Length - 1))
                        {
                            task.Context = token;
                            token = "";
                            pState = ParserState.end;
                            i--;
                        }
                        break;
                    case ParserState.comment:
                        i = line.Length;
                        break;
                    case ParserState.end:
                        break;

                }
            }
            if ( (pState == ParserState.end) || (pState == ParserState.project) )
            {
                // convert LineParsed to Task
                //Console.WriteLine(task);
                return new Line(task);
            }
            else if ( pState == ParserState.comment )
            {
                return null;
            }
            else
            {
                throw new Exception("parsing error .  line " + line );
            }
        }

    }

    class Filtering
    {
        public List<Line> Filter(List<Line> tasks, List<FilterCriteria> filterCriterias)
        {

            List<Line> result = new List<Line>();
            foreach (FilterCriteria fc in filterCriterias)
            {
                List<Line> q = new List<Line>();
                switch (fc.Condition)
                {
                    case FilterCriteria.Operator.Equal:
                        q = tasks.FindAll(x => x.Project.Equals(fc.ValueLeft));
                        q.Sort((x, y) => x.InitDate.CompareTo(y.InitDate));
                        break;
                }
                result.AddRange(q);
            }

            return result;
        }
    }



    class Report
    {
        public void ReportA ( List<Line> tasks )
        {
            // generar informes
            
            List<FilterCriteria> filterCriterias = new List<FilterCriteria>();
            filterCriterias.Add(new FilterCriteria() { Criteria = FilterCriteria.Fields.project, ValueLeft = "+SCA", ValueRight = "+SCA", Condition = FilterCriteria.Operator.Equal });
            filterCriterias.Add(new FilterCriteria() { Criteria = FilterCriteria.Fields.project, ValueLeft = "+BSM_VMP", ValueRight = "+BSM_VMP", Condition = FilterCriteria.Operator.Equal });
            filterCriterias.Add(new FilterCriteria() { Criteria = FilterCriteria.Fields.project, ValueLeft = "+GESTIO", ValueRight = "+GESTIO", Condition = FilterCriteria.Operator.Equal });

            Filtering filtering = new Filtering();
            List<Line> tasksFiltered = filtering.Filter(tasks, filterCriterias);
            

            double totalMinutes = 0;
            foreach (Line t in tasksFiltered)
            {
                totalMinutes += t.Minutes();
                Console.WriteLine("------------------------");
                Console.WriteLine(t);
                Console.WriteLine("------------------------\n\n");
            }

            Console.WriteLine("#########################\n\n");
            Console.WriteLine("Total Minutes : " + totalMinutes);
            Console.WriteLine("Total Hours   : " + totalMinutes / 60);
        }

        public List<string> getAllProjects ( List<Line> tasks )
        {
            List<string> projects = new List<string>();
            foreach ( Line task in tasks)
                if (!projects.Contains(task.Project)) projects.Add(task.Project);
            return projects;
        }

        public double getTotalMinutesFromProject (List<Line> tasks, string project )
        {
            List<FilterCriteria> filterCriterias = new List<FilterCriteria>();
            filterCriterias.Add(new FilterCriteria() { Criteria = FilterCriteria.Fields.project, ValueLeft = project, ValueRight = project, Condition = FilterCriteria.Operator.Equal });
            Filtering filtering = new Filtering();
            List<Line> tasksFiltered = filtering.Filter(tasks, filterCriterias);
            double totalMinutes = 0;
            foreach (Line t in tasksFiltered)
                totalMinutes += t.Minutes();

            return totalMinutes;
        }

        public void ReportAllTaskFromProject(List<Line> tasks, string project)
        {
            // generar informes
            List<FilterCriteria> filterCriterias = new List<FilterCriteria>();
            filterCriterias.Add(new FilterCriteria() { Criteria = FilterCriteria.Fields.project, ValueLeft = project, ValueRight = project, Condition = FilterCriteria.Operator.Equal });

            Filtering filtering = new Filtering();
            List<Line> tasksFiltered = filtering.Filter(tasks, filterCriterias);

            double totalMinutes = 0;
            foreach (Line t in tasksFiltered)
            {
                totalMinutes += t.Minutes();
                Console.WriteLine("------------------------");
                Console.WriteLine(t);
                Console.WriteLine("------------------------");
            }

            Console.WriteLine("#########################");
            Console.WriteLine("Total Minutes : " + totalMinutes);
            Console.WriteLine("Total Hours   : " + totalMinutes / 60);
        }


    }


    //-- ------------------------------------------------------------------------ --//
    // MAIN
    class Program
    {
        static void Main(string[] args)
        {
            string filePath;
            if (args.Length != 0)
                filePath = args[0];
            else
                filePath = "C:\\PROJECTES\\Personal\\todo.txt";
            Parser p = new Parser();
            List<Line> tasks = p.ParseFile(filePath);

            Report report = new Report();
            //report.ReportA( tasks  );

            Console.WriteLine("Projects: ");
            List<string> projects = report.getAllProjects(tasks);
            foreach ( string project in projects )
                Console.Write(project + ",");

            // report total minutes from project
            Console.WriteLine("\n[----------------------------------------------------------------------------------------------------------------------------]");
            foreach (string project in projects)
            {
                double totalMinutes = report.getTotalMinutesFromProject(tasks, project);
                string s1 = "PROJECT: " + project.PadLeft(40) + " || Minutes: ".PadLeft(5) + totalMinutes +";";
                string s2 = "Hours: " + totalMinutes / 60;
                s2 = s2.PadLeft(120 - s1.Length);
                Console.WriteLine(s1 + s2);
            }
            Console.WriteLine("[----------------------------------------------------------------------------------------------------------------------------]");


            foreach ( string project in projects)
            {
                Console.WriteLine("\n\n\n");
                Console.WriteLine("####################################################");
                Console.WriteLine(project);
                report.ReportAllTaskFromProject(tasks, project);
            }

        }

    }

}
